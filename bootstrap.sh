#!/usr/bin/env bash

ADOPT=$1

source ./scripts/functions.sh

mkdir -p programs

stow $ADOPT -vSt ~ bash zsh htop xinit

cd git

ask "work env?"
if [[ $? -eq 1 ]]; then
	stow $ADOPT -vSt ~ work
else
	stow $ADOPT -vSt ~ personal
fi

cd ../

ask "desktop env?"
if [[ $? -eq 1 ]]; then
    # Make sure directory exists
    mkdir -p ~/.composer ~/.config/Code/User ~/.config/autostart ~/.vim ~/.vim/templates ~/.rofi ~/.tig
    stow $ADOPT -vSt ~ shortcuts flameshot vim vifm composer php rofi tig

    sudo stow $ADOPT -vSt /opt programs
    mkdir -p ~/Pictures/Wallpapers
    stow -vSt ~/Pictures/Wallpapers background
fi

source ~/.zshrc
