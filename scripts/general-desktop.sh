#!/usr/bin/env bash

source ./scripts/functions.sh

# Spotify
installed "spotify"
if [[ $? -eq 1 ]]; then
    echo "Installing spotify"
    curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
    echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
fi

# Typora
installed "typora"
if [[ $? -eq 1 ]]; then
    echo "Installing typora"
    wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
    sudo add-apt-repository -y 'deb https://typora.io/linux ./'
fi

# Signal
installed "signal-desktop"
if [[ $? -eq 1 ]]; then
    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
    sudo mv signal-desktop-keyring.gpg /usr/share/keyrings/

    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
fi

# Libreoffice
sudo add-apt-repository -y ppa:libreoffice/ppa

sudo apt -qq update -y

packagelist=(
  spotify-client

  libreoffice
  thunderbird
  qbittorrent
  signal-desktop
  firefox
  rofi
  # Markdown editor
  typora

  # Disk Usage Analyzer
  baobab

  # Background manager
  nitrogen

  # Transparant terminal
  compton

  # Requirement Whatpulse
  libssl-dev
  libqt5widgets5
  libqt5sql5

  # Requirements DWM
  libx11-dev
  libxinerama-dev
  libxft-dev

  # Requirements Slock
  libxrandr-dev
  libimlib2-dev
)

echo "Installing package list"
sudo apt -qq install -y ${packagelist[@]}

snap install vlc

# Add user to groups.
# Write isos
sudo adduser $USER disk
# Arduino
sudo adduser $USER dialout

# Install teamviewer
installed "teamviewer"
if [[ $? -eq 1 ]]; then
    curl -OL https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
    # Run once so missing require packages get detected
    sudo dpkg -i teamviewer_amd64.deb
    # Install required packages
    sudo apt-get -f install
    # This time teamviewer will install
    sudo dpkg -i teamviewer_amd64.deb
fi

# Install flameshot
installed "flameshot"
if [[ $? -eq 1 ]]; then
  curl -OL https://github.com/lupoDharkael/flameshot/releases/download/v0.6.0/flameshot_0.6.0_bionic_x86_64.deb
  sudo dpkg -i flameshot_0.6.0_bionic_x86_64.deb
fi

# Install whatpulse
cd programs
sudo curl -L https://whatpulse.org/downloads/313/64bit/ -o whatpulse.tar.gz
sudo mkdir -p whatpulse
yes $USER | sudo tar xvzf whatpulse.tar.gz -C whatpulse
sudo rm -f whatpulse.tar.gz

# Install nextcloud
# if [ ! -f "../programs/nextcloud.AppImage" ]; then
#   curl -L https://download.nextcloud.com/desktop/releases/Linux/latest -o ../programs/nextcloud.AppImage
#   sudo chmod +x /opt/nextcloud.AppImage
#   sudo ln -s /opt/nextcloud.AppImage /usr/local/bin/nextcloud
# fi


# Install AppImageLauncher
installed "ail-cli"
if [[ $? -eq 1 ]]; then
    https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb
  sudo dpkg -i appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.debfi
fi
