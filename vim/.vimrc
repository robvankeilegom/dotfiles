
" Auto enable / install plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
    Plug 'lifepillar/vim-solarized8'

    Plug 'itchyny/lightline.vim'

    " Git wrapper
    Plug 'tpope/vim-fugitive'

    Plug 'tpope/vim-surround'

    Plug 'tpope/vim-commentary'

    " Plug 'joshdick/onedark.vim'

    " Languages
    Plug 'posva/vim-vue'

    " Plug 'pangloss/vim-javascript'
    " Plug 'komissarex/vim-progress'
    " Plug 'leafgarland/typescript-vim'
    " Plug 'stephpy/vim-php-cs-fixer'
    " Plug 'shawncplus/phpcomplete.vim'

    " Plug 'sheerun/vim-polyglot'
    Plug 'jwalton512/vim-blade'

    Plug 'ap/vim-css-color'

    "Plug 'ycm-core/YouCompleteMe'

    " Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

    Plug 'junegunn/fzf.vim'

    Plug 'cakebaker/scss-syntax.vim'

    Plug 'mattn/emmet-vim'

    Plug 'w0rp/ale'

    Plug 'phpactor/phpactor', {'for': 'php', 'branch': 'master', 'do': 'composer install --no-dev -o'}

    " Use release branch (recommend)
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-css', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-html', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-json', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-vetur', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-yaml', {'do': 'yarn install --frozen-lockfile'}
    Plug 'neoclide/coc-eslint', {'do': 'yarn install --frozen-lockfile'}
    " Plug 'neoclide/coc-phpls', {'do': 'yarn install --frozen-lockfile'}
    " Plug 'neoclide/coc-phpactor', {'do': 'yarn install --frozen-lockfile'}

    " Plug 'sonph/onehalf', { 'rtp': 'vim' }

    Plug 'liuchengxu/vista.vim'


    " Plug 'honza/vim-snippets'
call plug#end()

" Enable spellcheck
set spell spelllang=en_us

" Set compatibility to Vim only.
set nocompatible

" Helps force plug-ins to load correctly when it is turned back on below.
filetype off

" Turn on syntax highlighting.
syntax on

" Make background transparant
hi Normal guibg=NONE ctermbg=NONE

" For plug-ins to load correctly.
filetype plugin indent on

" Turn off modelines
set modelines=0

" Automatically wrap text that extends beyond the screen length.
set wrap

" Highlight current line
set cursorline

" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F2> key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>

" Uncomment below to set the max textwidth. Use a value corresponding to the width of your screen.
" set textwidth=79

" Disable autocommenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Set tabwith to 4
set tabstop=4
set softtabstop=4

" Set width to 4 when shifting lines
set shiftwidth=4

" Convert tabs to spaces
set expandtab
set noshiftround

" Enable smartindent
set smartindent

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Speed up scrolling in Vim
set ttyfast

" Status bar
set laststatus=2

" Display options
set showmode
set showcmd

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Display different types of white spaces.
set list
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number

" Set status line display
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ [BUFFER=%n]\ %{strftime('%c')}

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Enable incremental search
set incsearch

" Include matching uppercase words with lowercase search term
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

" Auto update file when changed on disk
set autoread

" Disable search highlighting
set nohlsearch

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
    " Recently vim can merge signcolumn and number column into one
    set signcolumn=number
else
    set signcolumn=yes
endif

" Store info from no more than 100 files at a time, 9999 lines of text, 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" Map the <Space> key to toggle a selected fold opened/closed.
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

" Automatically save and load folds
" autocmd BufWinLeave *.* mkview
" autocmd BufWinEnter *.* silent loadview"

" Turn relative line numbers on
set relativenumber

function! NearestMethodOrFunction() abort
      return get(b:, 'vista_nearest_method_or_function', '')
endfunction

" Set wombat theme for lightline
let g:lightline = {
            \ 'colorscheme': 'solarized',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \             [ 'gitbranch', 'readonly', 'filename', 'modified', 'method' ] ]
            \ },
            \ 'component_function': {
            \   'gitbranch': 'FugitiveHead',
            \   'method': 'NearestMethodOrFunction'
            \ },
            \ }

let g:solarized_termtrans = 1
let g:solarized_visibility = 'high'


" php-cs-fixer file
" let g:php_cs_fixer_config_file = '~/.php_cs.dist' " options: --config

" let g:phpactorPhpBin = "/usr/bin/php7.4"

" https://github.com/phpactor/phpactor/blob/master/ftplugin/php/mappings.vim
" au FileType php nmap <buffer> <Leader>u :PhpactorImportClass<CR>
" au FileType php nmap <buffer> <Leader>ua :PhpactorImportMissingClasses<CR>


" Remove trailing whitespaces on save
autocmd BufWritePre * :%s/\s\+$//e
" autocmd BufWritePost *.php silent! call PhpCsFixerFixFile()
au BufWritePost *.php silent! !./tools/php-cs-fixer/vendor/bin/php-cs-fixer fix <afile>
autocmd BufNewFile,BufRead *.cls,*.p,*.i  set syntax=progress

" Rebind buffer switch shortcuts
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

au BufWritePost *.php silent! !eval '[ -f ".git/hooks/ctags" ] && .git/hooks/ctags' &

" Disable swap files
set noswapfile

" Set leader to space
:let mapleader = " "

" Map ctrl + \ to toggle nerd tree
map <C-\> :NERDTreeToggle<CR>
map <leader>f :NERDTreeToggle %<cr>


" Set undo dir
set undodir=~/.vim/undodir
set undofile

" Git fugitive
nmap <leader>gst :G<CR>

" Buffer switching
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" Undotree
" nnoremap <leader>u :UndotreeShow<CR>

" Add maps for tab switching
nnoremap <C-h> :tabprevious<CR>
nnoremap <C-l> :tabnext<CR>

" Bindings for adding blank lines with enter in normal mode
map <S-Enter> O<ESC>
map <Enter> o<ESC>
" Bind CR to itself in quickfix window
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>

" Project search
nnoremap <leader>ps :Rg<SPACE>

autocmd FileType vue setlocal tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType javascript setlocal tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType yaml setlocal tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType typescript setlocal tabstop=2 shiftwidth=2 softtabstop=2

function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction

"autocmd FileType php inoremap <Leader>u <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>u :call PhpInsertUse()<CR>

" To disable pre-processor languages altogether (only highlight HTML, JavaScript, and CSS):
let g:vue_pre_processors = []

let g:ale_fixers = {
 \ 'javascript': ['eslint']
 \ }

let g:ale_sign_error = 'x'
let g:ale_sign_warning = '!'
let g:ale_fix_on_save = 1

" paste without overwriting register
vnoremap <leader>p "_dP

" yank and paste on system clipboard
noremap <Leader>Y "+y
noremap <Leader>P "+p

" Map fzf commands
" nnoremap <silent> <C-p> :Files<CR>

" Always enable preview window on the right with 60% width
" let g:fzf_preview_window = 'right:60%'

" [Buffers] Jump to the existing window if possible
" let g:fzf_buffers_jump = 1


" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-@> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
    inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    else
        call CocAction('doHover')
    endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>


augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder.
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

autocmd FileType php nmap <leader>s  :Vista finder<CR>

function! OrganizeImport()
    call CocAction('runCommand', 'editor.action.organizeImport')
endfunction

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

augroup templates
    autocmd BufNewFile *.vue 0r ~/.vim/templates/skeleton.vue
augroup END

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Make <tab> used for trigger completion, completion confirm, snippet expand and jump like VSCode.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

autocmd FileType php noremap <C-t> :call phpactor#Transform()<CR>

source ~/.vimrc.colors

colorscheme solarized8_high
autocmd vimenter * ++nested colorscheme solarized8
