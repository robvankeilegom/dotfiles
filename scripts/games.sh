#!/usr/bin/env bash

source ./scripts/functions.sh

# Discord
installed "discord"
if [[ $? -eq 1 ]]; then
  wget https://discordapp.com/api/download\?platform\=linux\&format\=deb -O discord.deb
  sudo dpkg -i discord.deb
  rm discord.deb
fi

# Steam
installed "steam"
if [[ $? -eq 1 ]]; then
  wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb -O steam.deb
  sudo dpkg -i steam.deb
  rm steam.deb
fi

# MultiMC
installed "multimc"
if [[ $? -eq 1 ]]; then
  wget https://files.multimc.org/downloads/multimc_1.4-1.deb -O multimc.deb
  sudo dpkg -i multimc.deb
 rm multimc.deb
fi

# Runescape
installed "runescape"
if [[ $? -eq 1 ]]; then
    wget -O - https://content.runescape.com/downloads/ubuntu/runescape.gpg.key | sudo apt-key add -
    sudo mkdir -p /etc/apt/sources.list.d
    echo "deb https://content.runescape.com/downloads/ubuntu trusty non-free" | sudo tee /etc/apt/sources.list.d/runescape.list > /dev/null
    apt -qq update
    apt -qq install -y runescape-launcher
fi
