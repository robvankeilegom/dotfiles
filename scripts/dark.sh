#!/bin/sh

sed -i 's/theme-light\.conf/theme.conf/' ~/.config/kitty/kitty.conf
sed -i 's/background=light/background=dark/' ~/.vimrc
