#!/usr/bin/env bash
history="jira-history.txt"


main() {
    query=$( (tac "$history") | rofi  -dmenu -matching fuzzy -location 0 -p "Issue > " )

    if [[ -n "$query" ]]; then
      echo "$query" >> $history
      echo "$(tac $history | awk '!seen[$0]++' | tac | tail -n 100)" > $history
      url="https://jira.organi.be/browse/${query}"
      xdg-open "$url"
    else
      exit
    fi
}

main

exit 0
