#!/usr/bin/env bash

sudo snap install poedit

packagelist=(
  mysql-workbench
)

sudo apt install -y ${packagelist[@]} > /dev/null

mkdir -p programs
cd programs

# Postman
if [ ! -d "Postman" ]; then
  wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz > /dev/null
  tar -xf postman.tar.gz
  rm postman.tar.gz
fi

cd ../
