#!/usr/bin/env bash

packagelist=(
  # read-write NTFS driver for Linux
  ntfs-3g
  # do not delete main-system-dirs
  safe-rm
  # default for many other things
  build-essential
  make
  # unzip, unrar etc.
  zip
  unzip
  rar
  unrar
  # interactive processes viewer
  htop
  # interactive I/O viewer
  iotop

  rsync
  stow

  # command line clipboard
  xclip
  # more colors in the shell
  grc
  # fonts also "non-free"-fonts
  # -- you need "multiverse" || "non-free" sources in your "source.list" --
  fontconfig

  # get files from web
  wget
  curl
  # usefull tools
  nmap
  zsh
  neofetch

  # install af for fzf
  silversearcher-ag

  ripgrep


  tmux
)

sudo apt install -y ${packagelist[@]}

# # set zsh
# chsh -s $(which zsh)

# # oh-my-zsh
if [ ! -d "~/.oh-my-zsh" ]; then
  sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# youtube-dl
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl


